BOT_NAME = 'eTakoo'

SPIDER_MODULES = ['eTakoo.spiders']
NEWSPIDER_MODULE = 'eTakoo.spiders'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False


USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.100 Safari/537.36'

#  the numbers presents the order of the execution of the pipelines their wish are arbitrary
ITEM_PIPELINES = {'eTakoo.pipelines.KaymuPipeline': 1000}

TELNETCONSOLE_PORT = None

# MongoDB Jumia database configuration
MONGODB_JUMIA_SERVER = "ds119788.mlab.com"
MONGODB_JUMIA_PORT = 19788
MONGODB_DB_JUMIA = "jumia"

# MongoDB Kaymu(= jumia market) database configuration
MONGODB_KAYMU_SERVER = "ds119598.mlab.com"
MONGODB_KAYMU_PORT = 19598
MONGODB_DB_KAYMU = "kaymu"


