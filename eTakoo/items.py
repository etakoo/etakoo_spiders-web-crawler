
from scrapy import Item, Field


class ProductsItem(Item):

	# Product item's
	title = Field()
	price = Field()
	description = Field()
	urlImg = Field()
	supplier = Field()
	url = Field()
