# -*- coding: utf-8 -*-
import pymongo
from scrapy.conf import settings
from scrapy.exceptions import DropItem
import logging


class JumiaPipeline(object):
	def __init__(self):
		self.connection = pymongo.MongoClient(settings['MONGODB_JUMIA_SERVER'], settings['MONGODB_JUMIA_PORT'])
		self.db = self.connection[settings['MONGODB_DB_JUMIA']]
		self.db.authenticate('hatim', 'hatim')

	def process_item(self, item, spider):
		if item['supplier'] == 'jumia':
			collection_name = getattr(spider, 'category')
			collection = self.db[collection_name]
			logging.info(collection.insert(dict(item)))
		else:  # check the other pipeline
			return item


class KaymuPipeline(object):
	def __init__(self):
		self.connection = pymongo.MongoClient(settings['MONGODB_KAYMU_SERVER'], settings['MONGODB_KAYMU_PORT'])
		self.db = self.connection[settings['MONGODB_DB_KAYMU']]
		self.db.authenticate('hatim', 'hatim')

	def process_item(self, item, spider):
		if item['supplier'] == 'kaymu':
			collection_name = getattr(spider, 'category')
			collection = self.db[collection_name]
			logging.info(collection.insert(dict(item)))
		else:  # check the other pipeline
			return item










