from scrapy import Spider
from eTakoo.items import ProductsItem
import re


class KaymuSpider(Spider):
	name = "kaymu"

	def __init__(self):
		super(Spider, self).__init__()
		self.allowed_domains = ["https://market.jumia.ma/"]
		self.start_urls = [

			# # # computing
			'https://market.jumia.ma/ordinateurs-portables/',
			'https://market.jumia.ma/ordinateurs-de-bureau/',
			'https://market.jumia.ma/imprimantes-et-scanners/',
			# # # #  gaming
			'https://market.jumia.ma/jeux-video/',
			'https://market.jumia.ma/consoles-de-jeux-video/',
			# # # # mobile
			'https://market.jumia.ma/smartphones/',
			'https://market.jumia.ma/tablettes/',
			'https://market.jumia.ma/accessoires-pour-smartphone-et-tablettes/',
			# # # tv_video_audio
			'https://market.jumia.ma/appareil-photo-et-camera/',
			'https://market.jumia.ma/tvs/',
			# # # # # health_beauty
			'https://market.jumia.ma/maquillage/',
			'https://market.jumia.ma/soins-de-sante/',
			'https://market.jumia.ma/parfums-et-eau-de-cologne/',
			# # # # home_leaving
			'https://market.jumia.ma/mobilier/',
			'https://market.jumia.ma/electromenager/',
			# # # # # fashion
			'https://market.jumia.ma/vetements-femme/',
			'https://market.jumia.ma/accessoires-femme/',
			'https://market.jumia.ma/vetements-homme/',
			'https://market.jumia.ma/accessoires-homme/',
		]

		self.categories = {
			"computing": ['ordinateurs-portables', 'ordinateurs-de-bureau', 'imprimantes-et-scanners'],
			"gaming": ['jeux-video', 'consoles-de-jeux-video'],
			"mobile": ['smartphones', 'tablettes', 'accessoires-pour-smartphone-et-tablettes'],
			"tv_video_audio": ['appareil-photo-et-camera', 'tvs'],
			"health_beauty": ['maquillage', 'soins-de-sante''parfums-et-eau-de-cologne'],
			"home_leaving": ['electromenager', 'mobilier'],
			"fashion": ['vetements-femme', 'accessoires-femme', 'vetements-homme', 'accessoires-homme']
		}

	def parse(self, response):
		"""
		:param response:  the html body of th request to be parsed
		:return: a product
		"""

		products = response.xpath('//*[@id="kaymu"]/div[5]//div/article/a')
		for product in products:
			new_product = ProductsItem()
			new_product['title'] = product.xpath('section[1]/h3/text()').extract()[0]
			new_product['price'] = product.xpath('section[1]/div/span[1]/text()').extract()[0].split("s")[1].lstrip()
			new_product['description'] = ''
			new_product['urlImg'] = product.xpath('div/img/@data-layzr').extract()[0]
			new_product['url'] = 'https://market.jumia.ma' + product.xpath('@href').extract()[0]
			new_product['supplier'] = "kaymu"

			# get the products supplier and category
			product_category = self.get_product_category(response.url)
			category = product_category
			setattr(self, 'category', category)
			setattr(self, 'supplier', "kaymu")
			print new_product
			yield new_product

	def get_product_category(self, url):
		"""
			get the category info {supplier and the category } from the url
		:param url: the url of the products
		:return:  the category name of the product
		"""
		category_url = re.findall(r'/(.*?)/', url)[1]
		category = (key for key, values in self.categories.items() if category_url in values)
		return next(category)


