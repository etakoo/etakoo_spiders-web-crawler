from scrapy import Spider
from eTakoo.items import ProductsItem
import re


class JumiaSpider(Spider):
	name = "jumia"

	def __init__(self):
		super(Spider, self).__init__()
		self.allowed_domains = ["https://www.jumia.ma/"]
		self.start_urls = [
			# # # computing
			"https://www.jumia.ma/ordinateurs-accessoires-informatique/",
			# # # gaming
			"https://www.jumia.ma/jeux-videos-consoles/",
			# # # mobile
			"https://www.jumia.ma/telephone-tablette/",
			# # # tv_video_audio
			"https://www.jumia.ma/son-image-gps/",
			"https://www.jumia.ma/tv-home-cinema-lecteurs/",
			# # # health_beauty
			"https://www.jumia.ma/beaute-hygiene-sante/",
			"https://www.jumia.ma/sports-loisirs/",
			# # # home_leaving
			"https://www.jumia.ma/electromenager/",
			# # # fashion
			"https://www.jumia.ma/vestes-homme/", "https://www.jumia.ma/montres-hommes/",
			"https://www.jumia.ma/montres-femme/", "https://www.jumia.ma/vetements-femmes-mode/"
		]

		self.categories = {
			"computing":      ['ordinateurs-accessoires-informatique'],
			"gaming":         ['jeux-videos-consoles'],
			"mobile":         ['telephone-tablette'],
			"tv_video_audio": ['son-image-gps', 'tv-home-cinema-lecteurs'],
			"health_beauty":  ['beaute-hygiene-sante', 'sports-loisirs'],
			"home_leaving":   ['electromenager'],
			"fashion":        ['vestes-homme', 'montres-hommes', 'montres-femme', 'vetements-femmes-mode']
		}

	def parse(self, response):
		"""
		:param response:  the html body of th request to be parsed
		:return: a product
		"""
		products = response.xpath('/html/body/main/section[3]/section[3]//div/a')
		if products == [] or len(products) < 8:
			products = response.xpath('/html/body/main/section[2]/section[2]//div/a')
		if products == [] or len(products) < 8:
			products = response.xpath('/html/body/main/section[2]/section[3]//div/a')
		for product in products:
			new_product = ProductsItem()
			new_product['title'] = product.xpath('h2/span[2]/text()').extract()[0]
			new_product['price'] = product.xpath('span/span[1]/span[1]/text()').extract()[0]
			new_product['description'] = ''
			new_product['urlImg'] = product.xpath('div[2]/img/@data-src').extract()
			if not new_product['urlImg'] or new_product['urlImg'] == []:
				new_product['urlImg'] = product.xpath('div[1]/img/@data-src').extract()[0]
			# if the xpath returns more than 1 link => so we get the first one!
			elif len(new_product['urlImg']) >= 1:
				new_product['urlImg'] = product.xpath('div[2]/img/@data-src').extract()[0]
			new_product['url'] = product.xpath('@href').extract()[0]

			# get the products supplier and category
			product_category = self.get_product_category(response.url)
			category = product_category
			new_product['supplier'] = "jumia"
			setattr(self, 'category', category)
			setattr(self, 'supplier', "jumia")

			yield new_product

	def get_product_category(self, url):
		"""
			get the product category from the url from the url
		:param url: the url of the product
		:return:  the category name of the product
		"""
		category_url = re.findall(r'/(.*?)/', url)[1]
		category = (key for key, values in self.categories.items() if category_url in values)
		return next(category)
